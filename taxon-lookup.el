;;; -*- lexical-binding: t; -*-

(require 'widget)
(require 'sqlite)

(eval-when-compile
  (require 'wid-edit))

(defvar taxon-lookup--w)
(defvar taxon-lookup--search-using-w)
(defvar taxon-lookup-database-file "~/MyDocs/.documents/taxonomy.db")

(defun taxon-lookup-make-phonetic (lookup)
  (dolist (rule '(("\\(.\\)\\1" . "\\1")
		  ("[aiu]([aiu])[aiu]*" . "\\1")
		  ("[ou]" . "u")
		  ("[ye]" . "i")
		  ("ae" . "e")
		  ("[xz]" . "s")
		  ("[gcq]" . "k")
		  ("h" . "")
		  ("v" . "f")
		  ("ph" . "f")
		  ("g\\([ie]\\)" . "j\\1")
		  ("c\\([yie]\\)" . "z\\1")
		  ("-" . "")))
    (setq lookup (replace-regexp-in-string (car rule) (cdr rule) lookup)))
  lookup)

(defun taxon-lookup-function (lookup search-using)
  (let ((descriptor (sqlite-init taxon-lookup-database-file))
	(field (if (equal search-using "Phonetic")
		   "metaphone"
		 "epithet"))
	(lookup (if (equal search-using "Phonetic")
		    (taxon-lookup-make-phonetic lookup)
		  lookup))
	result)
    (unwind-protect
        (let ((rank (caar (sqlite-query descriptor (format "SELECT max(rank) FROM taxon WHERE %s LIKE '%s'" field lookup)))))
	  (when rank
	    (dolist (iter (sqlite-query descriptor (format "SELECT id FROM taxon WHERE %s LIKE '%s' AND rank=%s" field lookup rank)))
	      (let ((id (pop iter))
		    record parent-id accepted-id)
		(while id
		  (setq record (car (sqlite-query descriptor (format "SELECT accepted_id,parent_id,rank,epithet,authorship FROM taxon WHERE id=%s" id))))
		  (setq accepted-id (pop record))
		  (setq parent-id (pop record))		
		  (push record iter)
		  (setq id (or accepted-id parent-id)))
		(push (reverse iter) result))))
          (nreverse result))
      (sqlite-bye descriptor))))

(defun taxon-lookup ()
  "Create a Taxon Lookup form buffer.
In the form, enter a valid value for matching an epithet, and
click on the Search button."
  (interactive)
  (let ((on-search (lambda (&rest ignore)
		     (let ((response (taxon-lookup-function (widget-value taxon-lookup--w) (widget-value taxon-lookup--search-using-w))))
		       (let ((inhibit-read-only t))
			 (goto-char 1)
                         (forward-line 5)
                         (delete-region (point) (point-max)))
                       (dolist (derivation response)
                         (dolist (item derivation)
                           (widget-insert (format "%s  %s  %s\n" (car item) (cadr item) (caddr item))))
                         (widget-insert "\n"))
                       (goto-char 1)
                       (forward-line 2)
		       (widget-move 1)))))
    (switch-to-buffer "*Taxon Lookup*" nil t)
    (when (= 0 (buffer-size))
      (remove-overlays)
      (setq taxon-lookup--search-using-w
	    (widget-create 'radio-button-choice
			   :value "Epithet"
			   '(item "Epithet")
			   '(item "Common name")
			   '(item "Phonetic")))
      (setq taxon-lookup--w
	    (widget-create 'editable-field
			   :size 22
			   :format "Lookup: %v "))
      (widget-insert "  ")
      (widget-create 'push-button
		     :notify on-search
		     "Search") 
      (widget-insert "\n--- R E S U L T S ------------\n")
      (use-local-map widget-keymap)
      (widget-setup))
    (goto-char 1)
    (forward-line 2)
    (widget-move 1)))

(provide 'taxon-lookup)
