
(defmacro taxon-set-assoc-front (newelt place)
  "Change position in taxon derivation.
NEWELT is a tuple describing a taxon.  The first element is the
taxonomic rank, followed by whatever other relevant information.
PLACE is an associative list, containing ordered likewise formed
tuples of taxonomic rank (lower number is higher rank).  Use this
macro to switch to a different taxonomic derivation branch.  To
protect against PLACE becoming empty, either store a safe element
that won't be removed, such as '(-9999), or make sure you `setq
place to the result of this macro."
  `(progn
     (while (and ,place (>= (caar ,place) (car ,newelt)))
       (setq ,place (cdr ,place)))
     (if ,place
         (setq ,place (cons ,newelt ,place))
       (list ,newelt))))

(defun taxon-parse-org ()
  (interactive)
  (let ((db (sqlite-init taxon-lookup-database-file))
	(branch '((-99)))
	(current-line 0)
	line trailing-comment accepted-epithet
	id rank epithet authorship taxon res parent-id)
    (save-excursion
      (save-restriction
	(widen)
	(goto-char (point-min))
	(unwind-protect
	    (while (not (eobp))
	      (setq current-line (1+ current-line)
		    line (string-trim (thing-at-point 'line))
		    trailing-comment
		    (when (string-match "^\\(.*\\);;\\(.*\\)$" line)
			(prog1
			    (match-string 2 line)
			  (setq line (string-trim (match-string 1 line)))))
		    accepted-epithet
		    (when (string-match "^\\(.*\\) -+> \\([a-z]+\\)$" line)
		      (prog1
			  (match-string 2 line)
			(setq line (string-trim (match-string 1 line))))))
	      (cond ((string-match "^$" line) nil)
		    ((string-match "^\\(\\*+\\) \\([a-z]+\\)\\(?: \\([^\n]*\\)\\)?" line)
		     (setq rank (length (match-string 1 line))
			   epithet (match-string 2 line)
			   authorship (match-string 3 line))
		     ;; get id from database using epithet
		     (setq parent-id (cadr (assq (1- rank) branch))
			   res (sqlite-query db (format "select id from taxon where epithet='%s'" epithet)))
		     (if (null res)
			 (sqlite-query db (format "insert into taxon (rank,epithet,parent_id) values (%s,'%s',%s)" rank epithet parent-id))
		       (when parent-id
			 (sqlite-query db (format "update taxon set parent_id=%s where id=%s" parent-id (caar res)))))
		     (setq res (sqlite-query db (format "select id,authorship,epithet from taxon where epithet='%s'" epithet)))
		     (setq id (caar res))
		     (when (and authorship
				(not (equal authorship (cadar res))))
		       (sqlite-query db (format "update taxon set authorship='%s' where id=%s" authorship id)))
		     (setq taxon (list rank id epithet authorship))
		     (taxon-set-assoc-front taxon branch)
		     (message "%s %s %s %s" current-line branch rank line))
		    ((string-match "^\\([a-z]+\\)$" line)
		     (sqlite-query db (format "update taxon set parent_id=%s where epithet='%s' and rank=5" (cadr taxon) line))
		     (message "%s %s %s" current-line branch line)))
	      (when accepted-epithet
		(sqlite-query db (format "update taxon set accepted_id=(select id from taxon where epithet='%s') where id=%s" accepted-epithet id)))
	      (forward-line 1))
	  (sqlite-bye db)
	  (message "that's all folks!"))))))
